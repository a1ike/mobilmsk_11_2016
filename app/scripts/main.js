$(document).ready(function() {

  $('.carousel-inner div:first').addClass('active');

  $('#owl-oil').owlCarousel({

    autoPlay: 3000, //Set AutoPlay to 3 seconds
    pagination: false,
    navigation: false,
    items: 4,
    itemsDesktop: [1199, 3],
    itemsDesktopSmall: [979, 3],
    itemsDesktopSmall: [768, 1],
    navigationText: [
      '<img src=\'images/left.png\'>',
      '<img src=\'images/right.png\'>'
    ]

  });

  $('#owl-oil-2').owlCarousel({

    autoPlay: 3000, //Set AutoPlay to 3 seconds
    pagination: false,
    navigation: false,
    items: 4,
    itemsDesktop: [1199, 3],
    itemsDesktopSmall: [979, 3],
    itemsDesktopSmall: [768, 1],
    navigationText: [
      '<img src=\'images/left.png\'>',
      '<img src=\'images/right.png\'>'
    ]

  });

  $('#owl-oil-3').owlCarousel({

    autoPlay: 3000, //Set AutoPlay to 3 seconds
    pagination: false,
    navigation: false,
    items: 3,
    itemsDesktop: [1199, 3],
    itemsDesktopSmall: [979, 2],
    itemsDesktopSmall: [768, 1],
    navigationText: [
      '<img src=\'images/left.png\'>',
      '<img src=\'images/right.png\'>'
    ]

  });

  var owl = $('#owl-oil');

  owl.owlCarousel();

  // Custom Navigation Events
  $('.main-title-slider__right').click(function() {
    owl.trigger('owl.next');
  })
  $('.main-title-slider__left').click(function() {
    owl.trigger('owl.prev');
  });

  var owl2 = $('#owl-oil-2');

  owl2.owlCarousel();

  // Custom Navigation Events
  $('.main-title-slider__right').click(function() {
    owl2.trigger('owl.next');
  })
  $('.main-title-slider__left').click(function() {
    owl2.trigger('owl.prev');
  });

  $('.header').on('click', '.search-toggle', function(e) {
    var selector = $(this).data('selector');

    $(selector).toggleClass('show').find('.search-input').focus();
    $(this).toggleClass('active');

    e.preventDefault();
  });

  //$(function(){}) 為匿名註冊，不方便寫測試案例
  //當 Document Ready時 執行實際存在的 main()
  $(main());

  function main() {
    $('#menu-list .menu-name').mouseenter(function(event) {
      //點到.menu-name 尋找 ul 執行show
      //end 回到 $(this) 
      //siblings 尋找 同輩元素
      //尋找 ul 執行hide
      $(this).find('ul').show('fast').end().siblings().find('ul').hide('fast');
      //停止其他可能與 $('#menu-list .menu-name').click DOM 相關的事件
      //避免錯誤觸發
      event.stopPropagation();
    });
  }

});